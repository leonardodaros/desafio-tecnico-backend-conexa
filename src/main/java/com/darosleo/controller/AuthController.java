package com.darosleo.controller;

import com.darosleo.json.request.LoginRequest;
import com.darosleo.json.request.SignupRequest;
import com.darosleo.service.AuthService;
import com.darosleo.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class AuthController {

    @Autowired
    UsuarioService usuarioService;

    @Autowired
    AuthService authService;

    @PostMapping("/login")
    public ResponseEntity authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        return authService.authenticateUser(loginRequest);
    }

    @PostMapping("/signup")
    public ResponseEntity registerUser(@Valid @RequestBody SignupRequest signUpRequest) throws Exception {
        return usuarioService.registerUser(signUpRequest);
    }

    @PostMapping("/logoff")
    public ResponseEntity logoutUser() {
        return authService.logoutUser();
    }
}
