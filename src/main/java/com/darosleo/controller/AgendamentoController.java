package com.darosleo.controller;

import com.darosleo.json.request.AtendimentoRequest;
import com.darosleo.service.AgendamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/attendance")
public class AgendamentoController {

    @Autowired
    AgendamentoService agendamentoService;

    @PostMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity userAccess(@Valid @RequestBody AtendimentoRequest request) {
        return agendamentoService.criarAtendimento(request);
    }

}
