package com.darosleo.json.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PacienteRequest {

    @NotBlank
    @NotNull
    private String nome;

    @NotBlank
    @NotNull
    @CPF
    private String cpf;
}
