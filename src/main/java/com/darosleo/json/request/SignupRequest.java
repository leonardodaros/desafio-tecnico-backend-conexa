package com.darosleo.json.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignupRequest {

    @NotBlank
    @Size(max = 50)
    @Email
    @NotNull
    private String email;

    @NotBlank
    @Size(min = 6, max = 40)
    @NotNull
    private String senha;

    @NotBlank
    @Size(min = 6, max = 40)
    @NotNull
    private String confirmacaoSenha;

    @NotBlank
    @NotNull
    private String especialidade;

    @NotBlank
    @NotNull
    @CPF
    private String cpf;

    @NotBlank
    private String dataNascimento;

    @NotBlank
    @NotNull
    @Pattern(regexp = "(^$|[0-9]{10})")
    private String telefone;
}
