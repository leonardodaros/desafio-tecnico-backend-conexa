package com.darosleo.json.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AtendimentoRequest {

    @NotNull
    private String dataHora;

    @NotNull
    private PacienteRequest paciente;
}
