package com.darosleo.service;

import com.darosleo.json.request.SignupRequest;
import com.darosleo.json.response.MessageResponse;
import com.darosleo.model.Usuario;
import com.darosleo.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuarioService {

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    UsuarioRepository userRepository;

    @Transactional
    public ResponseEntity registerUser(SignupRequest signUpRequest) {

        if (existsUserByEmail(signUpRequest.getEmail())) {
            return ResponseEntity.badRequest().body(new MessageResponse("E-mail já está em uso!"));
        }
        if (!signUpRequest.getSenha().equals(signUpRequest.getConfirmacaoSenha())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Senha e confirmação não são iguais!"));
        }

        // Create new user's account
        Usuario usuario = Usuario.build(signUpRequest, encoder.encode(signUpRequest.getSenha()));

        save(usuario);

        return ResponseEntity.ok(new MessageResponse("Usuário registrado com sucesso!"));
    }

    @Transactional
    public Usuario loadUserByEmail(String email) throws UsernameNotFoundException {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado para o e-mail: " + email));
    }

    @Transactional
    public Boolean existsUserByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Transactional
    public Usuario save(Usuario usuario) throws UsernameNotFoundException {
        return userRepository.save(usuario);
    }
}
