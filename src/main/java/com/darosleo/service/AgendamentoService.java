package com.darosleo.service;

import com.darosleo.json.request.AtendimentoRequest;
import com.darosleo.json.response.MessageResponse;
import com.darosleo.model.Agendamento;
import com.darosleo.model.Paciente;
import com.darosleo.repository.AgendamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Service
public class AgendamentoService {

    @Autowired
    AgendamentoRepository agendamentoRepository;

    @Autowired
    PacienteService pacienteService;

    @Transactional
    public Agendamento save(Agendamento agendamento) throws UsernameNotFoundException {
        return agendamentoRepository.save(agendamento);
    }

    @Transactional
    public ResponseEntity criarAtendimento(AtendimentoRequest request) throws UsernameNotFoundException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(request.getDataHora(), formatter);
        if (dateTime.isBefore(LocalDateTime.now())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Data de atendimento não pode ser anterior a data atual!"));
        }
        Paciente paciente;
        if (request.getPaciente() != null) {
            Optional<Paciente> pacienteOptional = pacienteService.loadByCpf(request.getPaciente().getCpf());
            if (pacienteOptional.isPresent()) {
                paciente = pacienteOptional.get();
            } else {
                paciente = new Paciente();
                paciente.setNome(request.getPaciente().getNome());
                paciente.setCpf(request.getPaciente().getCpf());
                paciente = pacienteService.save(paciente);
            }

            Agendamento agendamento = new Agendamento();
            agendamento.setDataHora(dateTime);
            agendamento.setPaciente(paciente);

            Agendamento agendamentoSaved = save(agendamento);
            return ResponseEntity.ok(agendamentoSaved);
        } else {
            return ResponseEntity.badRequest().body("Dados inválidos do Paciente");
        }
    }

}
