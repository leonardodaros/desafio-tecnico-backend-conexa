package com.darosleo.service;

import com.darosleo.model.Paciente;
import com.darosleo.repository.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class PacienteService {

    @Autowired
    PacienteRepository pacienteRepository;

    @Transactional
    public Optional<Paciente> loadByCpf(String cpf) throws UsernameNotFoundException {
        return pacienteRepository.findByCpf(cpf);
    }

    @Transactional
    public Paciente save(Paciente paciente) throws UsernameNotFoundException {
        return pacienteRepository.save(paciente);
    }

}
