package com.darosleo.model;

import com.darosleo.json.request.SignupRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "usuario",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "email")
        })
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 50)
    @Email
    @NotNull
    private String email;

    @NotBlank
    @Size(max = 120)
    @NotNull
    private String senha;

    @NotBlank
    @Size(max = 120)
    private String especialidade;

    @NotBlank
    @Size(max = 15)
    private String cpf;

    @NotBlank
    private String dataNascimento;

    @NotBlank
    private String telefone;

    @NotNull
    @NotBlank
    @JsonIgnore
    private String role;

    public Usuario(String email, String senha) {
        this.email = email;
        this.senha = senha;
        this.role = "medico";
    }

    public Usuario(String email, String senha, String especialidade, String cpf, String dataNascimento, String telefone) {
        this.email = email;
        this.senha = senha;
        this.especialidade = especialidade;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.telefone = telefone;
        this.role = "medico";
    }

    public static Usuario build(SignupRequest request, String senhaEncoded) {
        return new Usuario(
                request.getEmail(),
                senhaEncoded,
                request.getEspecialidade(),
                request.getCpf(),
                request.getDataNascimento(),
                request.getTelefone()
        );
    }

}
