package com.darosleo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "paciente",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "cpf")
        })
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Paciente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @NotNull
    @Size(max = 120)
    @NotNull
    private String nome;

    @NotBlank
    @NotNull
    @Size(max = 15)
    private String cpf;

}
