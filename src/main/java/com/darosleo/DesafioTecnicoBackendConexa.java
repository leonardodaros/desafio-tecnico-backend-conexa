package com.darosleo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioTecnicoBackendConexa {

	public static void main(String[] args) {
		SpringApplication.run(DesafioTecnicoBackendConexa.class, args);
	}

}
